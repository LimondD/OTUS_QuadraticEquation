using NUnit.Framework;
using System;

namespace QuadraticEquationTests
{
    public class QuadraticEquationTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test(Description = "��� ��������� x^2+1 = 0 ������ ��� (������������ ������ ������)")]
        public void Solve_Empty_IfNoRoots()
        {
            //Arrange
            var education = new QuadraticEquation.QuadraticEquation();
            double a = 1;
            double b = 0;
            double c = 1;

            // Act
            var result = education.Solve(a, b, c);

            // Assert
            Assert.IsEmpty(result);
        }

        [Test(Description = "��� ��������� x^2-1 = 0 ���� ��� ����� ��������� 1 (x1=1, x2=-1)")]
        public void Solve_TwoRoots_IfDescriminantGreaterThenZero()
        {
            //Arrange
            var education = new QuadraticEquation.QuadraticEquation();
            double a = 1;
            double b = 0;
            double c = -1;

            // Act
            var result = education.Solve(a, b, c);

            // Assert
            Assert.That(result, Has.Exactly(2).Items);
            Assert.That(result, Has.Exactly(1).EqualTo(1)
                .And.Exactly(1).EqualTo(-1));
        }

        [Test(Description = "��������� ��� ���� ������ 1 ������ ���� ������������ = 0 ��� < epsilon")]
        [TestCase(1, 2, 1)]
        [TestCase(0.002, 0.00001, -0.0001)]
        public void Solve_OneRoot_IfDescriminantZero(double a, double b, double c)
        {
            //Arrange
            var education = new QuadraticEquation.QuadraticEquation();

            // Act
            var result = education.Solve(a, b, c);

            // Assert
            Assert.That(result, Has.Exactly(1).Items);
        }

        [Test(Description = "����������� a �� ����� ���� ����� 0. � ���� ������ solve ����������� ����������")]
        [TestCase(0.0, 2, 1)]
        [TestCase(1.0e-7, 0, -1)]
        public void Solve_Exception_If_a_Zero_Or_Less_Then_Epsilon(double a, double b, double c)
        {
            //Arrange
            var education = new QuadraticEquation.QuadraticEquation();

            // Act
            //Assert
            Assert.Throws<ArgumentException>(() => education.Solve(a, b, c));
        }

        [Test(Description = "��������� �������� ������������ �������� �� �����")]
        [TestCase(1, double.MaxValue, 1)]
        [TestCase(1, double.NegativeInfinity, 1)]
        [TestCase(1, 3, double.PositiveInfinity)]
        [TestCase(1, double.NaN, 1)]
        public void Solve_Exception_IfNotValid(double a, double b, double c)
        {
            //Arrange
            var education = new QuadraticEquation.QuadraticEquation();

            // Act
            //Assert
            Assert.Throws<Exception>(() => education.Solve(a, b, c));
        }
    }
}