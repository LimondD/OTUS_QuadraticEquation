﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuadraticEquation
{
    public class QuadraticEquation
    {
        readonly double epsilon = 1.0e-6;

        public double[] Solve(double a, double b, double c)
        {
            if (a < epsilon)
            {
                throw new ArgumentException("Коэффициент 'a' не может быть равным нулю меньше epsilon");
            }

            CheckDoubleValue(a, b, c);

            var descriminant = GetDiscriminant(a, b, c);
            CheckDoubleValue(descriminant);
            
            if (descriminant < 0.0)
            {
                return Array.Empty<double>();
            }
            else if (descriminant > 0.0 && descriminant > epsilon)
            {
                var firstRoot = (-b - Math.Sqrt(descriminant)) / 2 * a;
                var secondRoot = (-b + Math.Sqrt(descriminant)) / 2 * a;

                return new double[] { firstRoot, secondRoot };
            }
            // descriminant == 0 или < epsilon
            else
            {
                var root = (-b) / 2 * a;
                return new double[] { root };
            }
        }

        private void CheckDoubleValue(params double[] values)
        {
            foreach (var value in values)
            {
                if (double.IsInfinity(value) || double.IsNegativeInfinity(value) || double.IsNaN(value))
                {
                    throw new Exception("Неверное значение double");
                }
            }
        }

        private double GetDiscriminant(double a, double b, double c)
        {
            return Math.Pow(b, 2) - 4 * a * c;
        }
    }
}
